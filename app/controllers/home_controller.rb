class HomeController < ApplicationController
  def index
  end
  def shortest_path
  	@path = 0
  	@start_station_id = params[:from].to_i
  	@destination_station_id = params[:to].to_i
  	if (params[:from] != @destination_station_id)
  		from = Station.find(@start_station_id)
  		@hash_to_push = {}
  		@visiting_queue = []
  		@path = []
	  	@hash_to_push[@start_station_id.to_s] = 0
	  	@visiting_queue << @start_station_id
	  	find_path(from)
	  	if @success
	  		create_path(@destination_station_id)
	  		@path.reverse!
	  	end
	 end
  end

  def find_path(check_station)
  	# p "@hash_to_push", @hash_to_push
  	# p "@visiting_queue", @visiting_queue
  	
  	check_station.connections.each do|s|
  		if s.connect_id == @destination_station_id
  			@hash_to_push[s.connect_id.to_s] = check_station.id
  			@success = true
  			return
  		elsif !@hash_to_push.has_key?(s.connect_id.to_s)
  			@hash_to_push[s.connect_id.to_s] = check_station.id
  			@visiting_queue << s.connect_id
  		end
  	end
  	@visiting_queue.shift 
  	if @visiting_queue.length>0
  		find_path(Station.find(@visiting_queue[0]))
  	else
  		@success = false 
  		return
  	end
  end

  def create_path(key)
  	@path << Station.find(key).name
  	if key == @start_station_id
  		return
  	else
  		create_path(@hash_to_push[key.to_s])
  	end
  end

end
