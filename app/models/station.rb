class Station < ActiveRecord::Base
	has_many :connections

	def add_connects(stations_array)
		Station.where(:name=>stations_array).collect(&:id).each do |id|
			self.connections.build(:connect_id=>id)
		end
		self.save
	end
end
